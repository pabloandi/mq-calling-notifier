#!/usr/bin/python3
# -*- coding: utf-8 -*-

from lib.producer import Producer

class MainProducer(Producer):
    """docstring for MainProducer."""
    def __init__(self):
        super(MainProducer, self).__init__()

    def check_message(self, message):
        if message.application == 'MacroExit' and message.context == 'macro-confirm' and message.exten == '1':
            self.log.info("Someone has answered a client ...")
            self.queue_name = 'answered_calls'
            return True
        elif message.application == 'NoOp' and message.appdata == 'Returning since nobody answered':
            self.log.info("A lost call is reported ...")
            self.queue_name = 'lost_calls'
            return True
        elif message.application == 'Hangup' and message.channelstate == '4' and message.channelstatedesc == 'Ring':
            self.log.info("A client has hangup a call before an adviser answered it ...")
            self.queue_name = 'hangup_calls'
            return True
        else:
            return False



def main():
    producer = MainProducer()
    producer.run()

if __name__ == '__main__':
    main()
