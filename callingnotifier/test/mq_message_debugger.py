#!/usr/bin/python3
# -*- coding: utf-8 -*-

import asyncio
import logging as log
import configparser
from panoramisk import Manager

log.basicConfig(level=log.INFO, format='%(asctime)s : %(levelname)s : %(message)s')

config = configparser.ConfigParser()
try:
    config.read('../.env')
except Exception as e:
    log.error("Can't read the config file: %s", e)

#TODO finish it

if __name__ == '__main__':
    main()
