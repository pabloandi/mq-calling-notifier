#!/usr/bin/python3
# -*- coding: utf-8 -*-

import asyncio
import logging as log
import configparser
from panoramisk import Manager

log.basicConfig(level=log.INFO, format='%(asctime)s : %(levelname)s : %(message)s')

config = configparser.ConfigParser()
try:
    config.read('../.env')
except Exception as e:
    log.error("Can't read the config file: %s", e)

manager = Manager(loop=asyncio.get_event_loop(),
                  host=config['asterisk']['host'],
                  username=config['asterisk']['username'],
                  secret=config['asterisk']['secret'])

@manager.register_event('Newexten')
def callback(manager, message):
    print(message)

def main():
    manager.connect()
    try:
        manager.loop.run_forever()
    except KeyboardInterrupt:
        manager.loop.close()

if __name__ == '__main__':
    main()
