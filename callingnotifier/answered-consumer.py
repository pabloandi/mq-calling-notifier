#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pika
import pickle

from lib.consumer import Consumer
from lib.model.message import Message
from lib.model.adviser import Adviser

class AnsweredCallsConsumer(Consumer):

    queue_name = 'answered_calls'

    """docstring for LostConsumer."""
    def __init__(self):
        super(AnsweredCallsConsumer, self).__init__()

    def callback(self, ch, method, properties, body):
        try:
            self.log.info("unserializing AMI message data")
            self.message = pickle.loads(body)

            if self.message:
                # pass
                self.log.info(self.message.__dict__)
            else:
                raise Exception('No message found in AMI message')

        except Exception as e:
            self.log.error("Can't unserialize the AMI message and adviser data: %s" % e)

        try:
            self.adviser = Adviser(self.get_adviser_phone())
            self.send_email()

            self.log.info("done -> email: %s" % self.adviser.email)

            ch.basic_ack(delivery_tag = method.delivery_tag)

        except Exception as e:
            self.log.error("An error has ocurred: %s " % e)
            ch.basic_reject(delivery_tag = method.delivery_tag, requeue=False)

def main():
    consumer = AnsweredCallsConsumer()
    consumer.run()

if __name__ == '__main__':
    main()
