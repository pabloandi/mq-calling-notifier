#!/usr/bin/python3
# -*- coding: utf-8 -*-

import asyncio
import logging as log
import pika
import pickle
import sys

from lib import baseclass
from lib import model
from collections import namedtuple

base = baseclass.BaseClass()
log = base.log
config = base.config

serialized_message = None
connection = None

class Message(object):
    """docstring for Message."""
    def __init__(self, application,context,exten,connectedlinenum,channel,channelstate,channelstatedesc,linkedid,uniqueid):
        self.application = application
        self.context = context
        self.exten = exten
        self.connectedlinenum = connectedlinenum
        self.channel = channel
        self.channelstate = channelstate
        self.channelstatedesc = channelstatedesc
        self.linkedid = linkedid
        self.uniqueid = uniqueid


def send_message(message,queuename):
    connection = None
    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=config['rabbitmq']['host'], port=config['rabbitmq']['port']))
        channel = connection.channel()
        channel.queue_declare(queue=queuename, durable=True)
        channel.basic_publish(exchange='',routing_key=queuename,body=message,properties=pika.BasicProperties(delivery_mode=2))

    except pika.exceptions.ConnectionClosed as e:
        log.warning("Connection close by the broker: %s" % e)
        # Do not recover on channel errors
    except pika.exceptions.AMQPChannelError as e:
        log.warning("Channel error: %s" % e)
        # Recover on all other connection errors
    except pika.exceptions.AMQPConnectionError as e:
        log.warning("Connection error: %s" % e)
    finally:
        if connection:
            connection.close()


def main(t):

    serialized_message = None
    queuename = None

    if t == 'lost':
        #Lost messages
        m = Message('Hangup','macro-hangupcall','s','0000019143431120','PJSIP/101-00000023','7','Busy','1548691463.8450','1548691463.8450')
        queuename = 'lost_calls'
    elif t == 'answered':
        #Answered messages
        m = Message('MacroExit','macro-confirm','1','0000019143431120','Local/53155555555@from-internal-00000c84;2','7','Busy','1548691463.8450','1548691463.8450')
        queuename = 'answered_calls'
    elif t == 'hangup':
        #Hangup message
        m = Message('Hangup','macro-hangupcall','s','0000019143431120','PJSIP/101-00000023','4','Busy','1499565729.1619','1499565729.1619')
        queuename = 'hangup_calls'
    else:
        return

    serialized_message = pickle.dumps(m)
    send_message(serialized_message, queuename)

if __name__ == "__main__":
    main(sys.argv[1])