#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pika
import pickle
import time

from lib.consumer import Consumer
from lib.model.message import Message
from lib.model.adviser import Adviser
from lib.sender.webhooksender import WebhookSender

from pymysql.err import DataError as MySQLDataException

class LostCallsConsumer(Consumer):

    queue_name = 'lost_calls'

    """docstring for LostCallsConsumer."""
    def __init__(self):
        super(LostCallsConsumer, self).__init__()

    def send_notification(self):
        try:
            self.log.info("sending data to page %s" % self.config['request']['page'])

            try:
                client_phone = self.get_client_phone()
            except MySQLDataException as e:
                self.log.warning("warning: no client phone has been extracted from database, ignoring this message ...")
                return

            data = {
                'url': self.config['url']['1'], #TODO for now, only for the Raul's group
                'telefono': client_phone,
                'mensaje': 'Esta llamada no fue contestada por ningun asesor, por lo cual la hemos asignado a usted para que intente contactarse con un posible cliente. telefono: %s, fecha y hora: %s' % (client_phone, time.strftime('%Y-%m-%d %H:%M')),
                'l_lead_type': 3
            }

            WebhookSender(data)
        except Exception as e:
            self.log.error("Can't send the notification: %s" % e)
            raise

    def callback(self, ch, method, properties, body):
        try:
            self.log.info("unserializing AMI message data")
            self.message = pickle.loads(body)

            if self.message:
                # pass
                self.log.info(self.message.__dict__)
            else:
                raise Exception('No message found in AMI message')

        except Exception as e:
            self.log.error("Can't unserialize the AMI message and adviser data: %s" % e)

        try:
            # self.adviser = Adviser(self.get_adviser_phone())
            self.send_notification()

            self.log.info("done -> notification")

            ch.basic_ack(delivery_tag = method.delivery_tag)

        except Exception as e:
            self.log.error("An error has ocurred: %s " % e)
            ch.basic_reject(delivery_tag = method.delivery_tag, requeue=False)

def main():
    consumer = LostCallsConsumer()
    consumer.run()

if __name__ == '__main__':
    main()
