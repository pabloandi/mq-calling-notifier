#!/usr/bin/python3
# -*- coding: utf-8 -*-

from .model import message as message_class
from .baseclass import BaseClass

import asyncio
import pika
import pickle
from panoramisk import Manager

class Producer(BaseClass):
    asyncio_manager = None
    queue_name = 'default'

    """docstring for Producer."""
    def __init__(self):
        super(Producer, self).__init__()
        self.asyncio_manager = self.init_manager()

    def parse_message(self, message):
        return message_class.Message(message)

    def init_manager(self):
        self.log.info(self.config['rabbitmq']['host'])
        self.log.info(self.config['rabbitmq']['port'])

        return Manager(loop=asyncio.get_event_loop(),
                          host=self.config['asterisk']['host'],
                          username=self.config['asterisk']['username'],
                          secret=self.config['asterisk']['secret'])

    def check_message(self, message):
        raise NotImplementedError('Must implement this method')

    def run(self):

        @self.asyncio_manager.register_event('Newexten')
        def callback(manager, message):
            serialized_message = None
            connection = None
            m = message_class.Message(message)

            if not self.check_message(m):
                return

            try:
                serialized_message = pickle.dumps(m)
            except Exception as e:
                pass

            try:
                connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.config['rabbitmq']['host'], port=self.config['rabbitmq']['port']))
                channel = connection.channel()
                channel.queue_declare(queue=self.queue_name, durable=True)
                channel.basic_publish(exchange='',routing_key=self.queue_name,body=serialized_message,properties=pika.BasicProperties(delivery_mode=2))

            except pika.exceptions.ConnectionClosed as e:
                self.log.warning("Connection close by the broker: %s" % e)
                # Do not recover on channel errors
            except pika.exceptions.AMQPChannelError as e:
                self.log.warning("Channel error: %s" % e)
                # Recover on all other connection errors
            except pika.exceptions.AMQPConnectionError as e:
                self.log.warning("Connection error: %s" % e)
            finally:
                if connection:
                    connection.close()

        self.asyncio_manager.connect()
        try:
            self.asyncio_manager.loop.run_forever()
        except KeyboardInterrupt:
            self.asyncio_manager.close()
