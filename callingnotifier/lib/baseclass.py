#!/usr/bin/python3
# -*- coding: utf-8 -*-

from .config import Config
from .logger import logger as baselogger
from .logger import nativelogger
from .logger import sentrylogger
# from importlib import import_module


class BaseClass(object):
    config = None
    log = None

    """docstring for BaseClass."""
    def __init__(self):
        super(BaseClass, self).__init__()

        self.config = Config().config
        # logger_module = import_module(self.config['logger']['default'])
        # self.log = baselogger.Logger(logger_module.Logger())
        self.log = baselogger.Logger(nativelogger.Logger())
