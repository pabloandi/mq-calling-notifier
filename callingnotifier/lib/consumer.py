#!/usr/bin/python3
# -*- coding: utf-8 -*-

from .model import adviser
from smtplib import SMTPException
from .baseclass import BaseClass
from .sender.mailsender import MailSender
from .database.db import DB

from pymysql.err import DatabaseError as MySQLException
from pymysql.err import DataError as MySQLDataException

import pika
import re

class Consumer(BaseClass):

    message = None
    adviser = None
    connection = None
    channel = None
    queue_name = 'default'
    notification_message = 'default'

    """docstring fo Consumer."""
    def __init__(self):
        super(Consumer, self).__init__()

    def get_adviser_phone(self):
        try:
            self.log.info("extracting phone data from message.channel: %s" % self.message.channel)
            return re.compile(r"/(.*)@").findall(self.message.channel)[0]
        except IndexError as e:
            self.log.error("No adviser's phone. can't look email: %s" % e)
            raise

    def send_email(self):
        try:
            self.log.info("sending email to adviser")
            subject = "Formulario de contacto: Notificacion de llamada contestada"
            mailsender = MailSender()
            mailsender.send(self.adviser.email, subject, self.message.connectedlinenum)
        except SMTPException as e:
            self.log.error("Can't send email : %s" % e)
            raise

    def get_client_phone(self):
        """ Get client's phone from database """

        try:
            cdr_row = self.get_cdr_data_from_database()

            if cdr_row is None:
                raise MySQLDataException('No data has been retrieved from database')


            return cdr_row['src']
        except MySQLException as e:
            self.log.error('Error in connection to Database: %s' % e)
            raise
        except IndexError as e:
            self.log.error('No "src" index in cdr_row data: %s' % e)
            raise

    def get_cdr_data_from_database(self):
        """ Get cdr table data from database """
        db_connection = DB()
        query = "select * from cdr where uniqueid = '%s' and dst regexp '^[+]?5[[:digit:]]{9,12}$' and src regexp '^[+]?[[:digit:]]{9,12}$' limit 1"  % (self.message.uniqueid)
        self.log.debug(query)
        data = None
        
        try:
            cursor = db_connection.query(query)
            data = cursor.fetchone()
        except MySQLException as e:
            self.log.error("There's a problem in database: %s" % e)
            raise
        finally:
            cursor.close()
                
        return data

    def callback(self, ch, method, properties, body):
        raise NotImplementedError

    def run(self):
        try:
            connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.config['rabbitmq']['host'], port=self.config['rabbitmq']['port']))
            channel = connection.channel()
            channel.queue_declare(queue=self.queue_name, durable=True)
            channel.basic_qos(prefetch_count=1)
            channel.basic_consume(self.callback, queue=self.queue_name)
            channel.start_consuming()
            # Do not recover if connection was closed by broker
        except KeyboardInterrupt:
            self.log.info('Service stopped by user')
        except pika.exceptions.ConnectionClosed as e:
            self.log.warning("Connection close by the broker: %s" % e)
            # Do not recover on channel errors
        except pika.exceptions.AMQPChannelError as e:
            self.log.warning("Channel error: %s" % e)
            # Recover on all other connection errors
        except pika.exceptions.AMQPConnectionError as e:
            self.log.warning("Connection error: %s" % e)
