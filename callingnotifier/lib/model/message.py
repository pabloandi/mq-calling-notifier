#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging as log

class Message(object):
    """AMI message data."""
    
    def __init__(self, message):
        super(Message, self).__init__()

        try:
            self.application = message.application
            self.appdata = message.appdata
            self.context = message.context
            self.exten = message.exten
            self.connectedlinenum = message.connectedlinenum
            self.channel = message.channel
            self.channelstate = message.channelstate
            self.channelstatedesc = message.channelstatedesc
            self.linkedid = message.linkedid
            self.uniqueid = message.uniqueid
        except IndexError as e:
            log.error("Can't index the data to message object: %s" % e)
            raise
