#!/usr/bin/python3
# -*- coding: utf-8 -*-

from pymysql.err import DatabaseError as MySQLException
from ..database import db as bd

import logging as log

class Adviser(object):
    """Adviser data."""

    telefono = None
    nombre = None
    email = None
    code = None
    grupo = None

    def __init__(self, phone):
        super(Adviser, self).__init__()

        try:
            log.info("getting adviser data from database")
            data = self.get_data_from_database(phone)
        except MySQLException as e:
            log.error("No adviser's data. can't look email: %s" % e)
            raise

        self.telefono = data['telefono']
        self.nombre = data['nombre']
        self.email = data['email']
        self.code = data['code']
        self.grupo = data['grupo']

    def get_data_from_database(self, phone):
        db = bd.DB()
        cursor = db.query("SELECT * FROM asesores where telefono = %s LIMIT 1", (phone))
        return cursor.fetchone()
