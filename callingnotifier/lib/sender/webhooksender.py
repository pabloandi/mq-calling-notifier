#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..baseclass import BaseClass
import requests

class WebhookSender(BaseClass):
    """Notification sender for webhook endpoints."""

    def __init__(self, data):
        super(WebhookSender, self).__init__()
        self.send(data)

    def send(self, data):
        page = self.config['request']['page']
        try:
            response = requests.post(page, data = data)
        except Exception as e:
            self.log.error("An error has ocurred sending data to page: %s" % e)
            raise

        try:
            response.raise_for_status()
        except Exception as e:
            raise Exception("No se pudo enviar los datos a la página: respuesta negativa del servidor")
            
