#!/usr/bin/python3
# -*- coding: utf-8 -*-

import smtplib
from ..baseclass import BaseClass

from email.message import EmailMessage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from time import localtime, strftime

class MailSender(BaseClass):
    """docstring for MailSender."""
    def __init__(self):
        super(MailSender, self).__init__()


    def get_smtp_server(self):

        try:
            smtp_server = smtplib.SMTP_SSL(self.config['smtp']['host'], self.config['smtp']['port'])
            smtp_server.ehlo()
            smtp_server.login(self.config['smtp']['username'], self.config['smtp']['password'])
            return smtp_server
        except smtplib.SMTPHeloError as e:
            self.log.error("Server did not reply: %s" % e)
            raise
        except smtplib.SMTPAuthenticationError as e:
            self.log.error("Incorrect username/password combination: %s" % e)
            raise
        except smtplib.SMTPException as e:
            self.log.error("Authentication failed: %s" % e)
            raise


    def send(self, receiver, subject, client_phone):

        sender = self.config['smtp']['username']
        receivers = [receiver]

        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = sender
        msg['To'] = receiver
        now_date = strftime("%Y-%m-%d", localtime())
        now_time = strftime("%I:%M:%S %p", localtime())

        text = "Usted ha recibido una llamada del número: %s el día %s a las %s" % (client_phone, now_date, now_time)
        html = """\
        <html>
            <head></head>
            <body>
                <p>%(text)s</p>
                <p>Para llamar <a href='tel:%(client_phone)s'>%(client_phone)s</a></p>
            </body>
        </html>
        """ % { 'text':text, 'client_phone':client_phone }

        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, 'html')

        msg.attach(part1)
        msg.attach(part2)

        smtp_server = self.get_smtp_server()

        try:
            smtp_server.send_message(msg)
            self.log.info("Successfully sent email")
        except smtplib.SMTPException as e:
            self.log.error("Error: unable to send email : %s " % e)
            raise
        finally:
            smtp_server.close()
