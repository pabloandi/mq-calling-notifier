#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
import sentry_sdk

from .. import config
from sentry_sdk.integrations.logging import LoggingIntegration

class Logger(object):
    """docstring for SentryLogger."""
    log = None
    config = None

    def __init__(self):
        self.config = config.Config().config
        sentry_logging = LoggingIntegration(
            level=logging.INFO,        # Capture info and above as breadcrumbs
            event_level=logging.ERROR  # Send errors as events
        )
        sentry_sdk.init(
            dsn=self.config['sentry']['dsn'],
            integrations=[sentry_logging]
        )

        self.log = logging


    def info(self, message):
        self.log.info(message)

    def debug(self, message):
        self.log.debug(message)

    def error(self, message):
        self.log.error(message)

    def warning(self, message):
        self.log.warning(message)
