#!/usr/bin/python3
# -*- coding: utf-8 -*-

class Logger(object):
    """docstring for Logger."""
    log = None

    def __init__(self, logger):
        self.log = logger

    def info(self, message):
        self.log.info(message)

    def debug(self, message):
        self.log.debug(message)

    def error(self, message):
        self.log.error(message)

    def warning(self, message):
        self.log.warning(message)
