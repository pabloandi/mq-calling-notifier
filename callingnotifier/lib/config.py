#!/usr/bin/python3
# -*- coding: utf-8 -*-

import configparser
from pathlib import Path

class Config(object):
    config = None

    """docstring for Config."""
    def __init__(self):
        super(Config, self).__init__()

        env_path = Path(__file__).resolve().parents[1].joinpath('.env')

        self.config = configparser.ConfigParser()
        with open(str(env_path)) as ini_file:
            self.config.readfp(ini_file)
