#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..baseclass import BaseClass

import pymysql.cursors

#class DB(BaseClass, metaclass=Singleton):
class DB(BaseClass):

    connection = None
    _singleton = None

    """docstring for DB."""
    def __init__(self):
        super(DB, self).__init__()
        self.connection = self.connect()

    def __new__(cls, *args, **kwargs):
        if not cls._singleton:
            cls._singleton = super(DB, cls).__new__(cls, *args, **kwargs)
        return cls._singleton


    def connect(self):

        return pymysql.connect(host=self.config['mysql']['host'],    # your host, usually localhost
                         user=self.config['mysql']['user'],         # your username
                         passwd=self.config['mysql']['password'],  # your password
                         db=self.config['mysql']['db'],        # name of the data base
                         cursorclass=pymysql.cursors.DictCursor
                         )

    def query(self, sql, params = None):
        c = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(sql, params)
                return cursor
        except pymysql.err.Error as e:
            try:
                self.log.error("MySQL error [%d]: %s" % (e.args[0], e.args[1]))
            except IndexError:
                self.log.error("MySQL error: %s" % str(e))
            finally:
                raise pymysql.err.Error
        finally:
            self.connection.close()
