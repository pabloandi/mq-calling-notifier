#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pika
import pickle
import time

from lib.consumer import Consumer
from lib.model.message import Message
from lib.model.adviser import Adviser
from lib.sender.webhooksender import WebhookSender

from pymysql.err import DataError as MySQLDataException

class HangupCallsConsumer(Consumer):
    """docstring for HangupConsumer."""

    queue_name = 'hangup_calls'
    last_message_linkedid = None
    
    def __init__(self):
        super(HangupCallsConsumer, self).__init__()

    
    def check_if_current_message_is_equal_to_last(self):
        if self.get_last_message_linkedid() == self.message.linkedid:
            return True
        else:
            return False
        
    def get_last_message_linkedid(self):
        return self.last_message_linkedid
    
    def set_last_message_linkedid(self, linkedid):
        self.last_message_linkedid = linkedid
    
    def send_hangup_notification(self):
        
        if self.check_if_current_message_is_equal_to_last():
            self.log.info("the current message is the same than last, then is ignored")
            return

        try:
            self.log.info("sending data to page %s" % self.config['request']['page'])

            try:
                client_phone = self.get_client_phone()
            except MySQLDataException as e:
                self.log.warning("warning: no client phone has been extracted from database, ignoring this message ...")
                return
            
            data = {
                'url': self.config['url']['4'],
                'telefono': client_phone,
                'mensaje': 'Esta llamada fue colgada por el cliente, por lo cual la hemos asignado a usted para que intente contactarse. telefono: %s, fecha y hora: %s' % (client_phone, time.strftime('%Y-%m-%d %H:%M')),
                'l_lead_type': 4
            }
            
            WebhookSender(data)

            self.set_last_message_linkedid(self.message.linkedid)

        except Exception as e:
            self.log.error("Can't send the hangup call notification: %s" % e)
            raise

    def callback(self, ch, method, properties, body):
        try:
            self.log.info("unserializing AMI message data")
            self.message = pickle.loads(body)

            if self.message:
                # pass
                self.log.info(self.message.__dict__)
            else:
                raise Exception('No message found in AMI message')

        except Exception as e:
            self.log.error("Can't unserialize the AMI message and adviser data: %s" % e)

        try:
            self.send_hangup_notification()

            self.log.info("done -> notification")

            ch.basic_ack(delivery_tag = method.delivery_tag)

        except Exception as e:
            self.log.error("An error has ocurred: %s " % e)
            ch.basic_reject(delivery_tag = method.delivery_tag, requeue=False)

def main():
    consumer = HangupCallsConsumer()
    consumer.run()

if __name__ == '__main__':
    main()
